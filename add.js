const addTo = (a) => {
    return (b) => {
        return a + b;
    };
};

const addToTen = addTo(10);
const addSeven = addToTen(7);

console.log(addSeven);